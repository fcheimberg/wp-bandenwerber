<?php
/**
* Plugin Name: wp-fcheimberg
* Description: Mit dem Plugin werden die Bandenwerber und Supporter angezeigt. Gelesen werden die Daten aus der DB. Dokumentation ist auf der unteren URL zu finden.
* Version: 1.3.1
* Author: Wittwer-Informatik - Simon
* Author URI: https://simuuh.ch/projekte/fc-heimberg-wordpress-plugin/
*/

/**
 * ToDo for Version 1.3.1
 * - Beheben der falschen URL weiterleitung nachdem der Spielfeldstatus angepasst wurde - DONE
 * - Success Meldung nachdem der Spielfeldstatus angepasst wurde
 * - Anpassen des Spielfeldstatus mit jQuery anstelle PHP Seite neuladen
 * - Die Namen der Bandenwerber / Supporter soll Alphabetisch abesteigend sortiert werden A,B ... Z
 * - CSS Problem beheben - Einige Theme-Klassen werden überschrieben. - DONE
 */


function fch_plugin_setup_menu(){
    add_menu_page( 'FC Heimberg Menü', 'FC Heimberg', 'upload_files', 'fch_plugin', 'fch_plugin_init', '', 76 );
}

function fch_plugin_init(){
    ?>
    <div class="wrap">
    <h1>FC Heimberg - Plugin Einstellungen</h1>
    <h5>Anpassen des Spielfeld Status</h5>
    <p>In diesem Bereich können die Einstellungen zu den beiden Spielfelder geändert werden. Falls ein neues Feld benötigt wird, bitte <a href="mailto:webseite@fcheimberg.ch">Simon</a> kontaktieren.
    <form action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>" method="post">
        <input type="hidden" name="action" value="process_form">
        <b>Hauptfeld</b><br />
        <input type="radio" name="hauptfeld" value="1"> Offen <br />
        <input type="radio" name="hauptfeld" value="0"> Geschlossen<br />
        <br />
        <b>Trainingsfeld</b><br />
        <input type="radio" name="trainingsfeld" value="1"> Offen <br />
        <input type="radio" name="trainingsfeld" value="0"> Geschlossen<br />
        <br />
        <?php submit_button( 'Speichern' ) ?>
    </form>
    <hr />
    <h5>Beschreibung</h5>
    <p>Mit den Tags [bandenwerber], [supporter] und [spielfeldstatus] können die entsprechenden Daten auf Seiten oder Widgets innerhalb der "FC Heimberg" Webseite eingebunden werden.</p>
    </div>
    <?php    
}


/*
 * wp_supporter_shortcode
 * 
 * Shortcode: [bandenwerber]
 *
 * Displays the "Supporter" as BOOTSTRAP PANELS
 *
 * @param (void)
 * @return (array) sql request result as array
 */
function wp_bandenwerber_shortcode(){
    // READ DATA FROM DB
    $bandenwerber = getDataFromDB("bandenwerber");

    // CHECK IF ARRAY IS EMPTY
   if (count($bandenwerber) != 0 )
   {
       ob_start();
        // CREATE BOOTSTRAP DIV ROW
        echo "<div class='row'>";

        // LOOP THRU THE BANDENWERBER ARRAY
        for ($i=0; $i < count($bandenwerber); $i++) 
        {   
            // DISPLAY THE BOOTSTRAP ROW
            echo "<div class='col-sm-6'>";
            echo "<div class='panel panel-default'>";
            echo "<div class='panel-body'>";    
            echo "<b>" . $bandenwerber[$i]['Vorname'] . " " . $bandenwerber[$i]['Name'] . "</b><br />";
            echo $bandenwerber[$i]['PLZ'] . " " . $bandenwerber[$i]['Ort'];
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
        ?>
        </div> <!-- END ROW -->
        <?php
        return ob_get_clean();
   }
}

/*
 * wp_supporter_shortcode
 * 
 * Shortcode: [supporter]
 *
 * Displays the "Supporter" as BOOTSTRAP PANELS
 *
 * @param (void)
 * @return (array) sql request result as array
 */
function wp_supporter_shortcode(){
    // READ DATA FROM DB
    $supporter = getDataFromDB("supporter");

    // CHECK IF ARRAY IS EMPTY
   if (count($supporter) != 0 )
   {
       ob_start();
        // CREATE BOOTSTRAP DIV ROW
        echo "<div class='row'>";

        // LOOP THRU THE BANDENWERBER ARRAY
        for ($i=0; $i < count($supporter); $i++) 
        {   
            // DISPLAY THE BOOTSTRAP ROW
            echo "<div class='col-sm-6'>";
            echo "<div class='panel panel-default'>";
            echo "<div class='panel-body'>";    
            echo "<b>" . $supporter[$i]['Vorname'] . " " . $supporter[$i]['Name'] . "</b><br />";
            echo $supporter[$i]['PLZ'] . " " . $supporter[$i]['Ort'];
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
        ?>
        </div> <!-- END ROW -->
        <?php
        return ob_get_clean();
   }
}

/*
 * wp_ehrenmitglieder_shortcode
 * 
 * Shortcode: [ehrenmitglieder]
 *
 * Displays the "Ehrenmitglieder" as BOOTSTRAP PANELS
 *
 * @param (void)
 * @return (array) sql request result as array
 */
function wp_ehrenmitglieder_shortcode(){
    // READ DATA FROM DB
    $ehrenmitglieder = getDataFromDB("ehrenmitglieder");
    ob_start();
    // CHECK IF ARRAY IS EMPTY
   if (count($ehrenmitglieder) != 0 )
   {
        // CREATE BOOTSTRAP DIV ROW
        echo "<div class='row'>";

        // LOOP THRU THE BANDENWERBER ARRAY
        for ($i=0; $i < count($ehrenmitglieder); $i++) 
        {   
            // DISPLAY THE BOOTSTRAP ROW
            echo "<div class='col-sm-6'>";
            echo "<div class='panel panel-default'>";
            echo "<div class='panel-body'>";    
            echo "<b>" . $ehrenmitglieder[$i]['Vorname'] . " " . $ehrenmitglieder[$i]['Name'] . "</b><br />";
            echo $ehrenmitglieder[$i]['PLZ'] . " " . $ehrenmitglieder[$i]['Ort'];
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
        ?>
        </div> <!-- END ROW -->
        <?php
        return ob_get_clean();
   }
}

/*
 * wp_spielfeldstatus
 *
 * Displays the Spielfeldstatus as an RED and GREEN traffic light
 * 
 * @since 1.0.0
 *
 * @param (void)
 * @return (void)
 */
function wp_spielfeldstatus(){
    $spielfeldstatus = getDataFromDB("spielfeldstatus");
    //print_r($spielfeldstatus);

    // https://wordpress.stackexchange.com/questions/75554/shortcode-output-appears-before-post-body
    ob_start();
    if (count($spielfeldstatus > 0)) 
    {
        echo $spielfeldstatus[0]['feldname'] ." = ". getStatus($spielfeldstatus[0]['status']) . " <br />";
        echo $spielfeldstatus[1]['feldname'] ." = " . getStatus($spielfeldstatus[1]['status']) ." <br />";
    } 
    else 
    {
        echo "No Data found, please check DB Connection!";
    }

    return ob_get_clean();
}

function getStatus($statusInt){
    if ($statusInt == 1) {
        return "Offen";
    }
    
    return "Geschlossen";

}

/*
 * getDataFromDB
 * 
 * Returns the sql result as an array.
 * 
 * @since 1.0.0
 *
 * @param (string) 'bandenwerber' or 'supporter' or 'spielfeldstatus'
 * @return (array) sql request result as array
 */
function getDataFromDB($filter_string){
    global $wpdb;
    $filter = "";
    $sektion = 0;

    switch ($filter_string) {
        case 'bandenwerber':
            $filter = "BW = 1";
            $sektion = 1;
            break;
        case 'supporter':
            $filter = "S = 1";
            $sektion = 1;
            break;
        case 'spielfeldstatus':
            $filter = "kein";
            $sektion = 2;
            break;
        case 'ehrenmitglieder':
            $filter = "M = 'EM'";
            $sektion = 1;
            break;
        default:
            $filter = "";
            $sektion = 0;
            break;
    }

    // DB QUERY
    if ($sektion == 1) {
        $results = $wpdb->get_results("SELECT `Vorname`, `Name`, `Adresse`, `PLZ`, `Ort` FROM membersyncoriginal WHERE $filter", ARRAY_A);
    }

    if ($sektion == 2) {
        // Change to correct statement
        $results = $wpdb->get_results("SELECT * FROM tbl_spielfeldstatus", ARRAY_A);
    }
    
    return $results;
}

function updateSpielfeldStatus($hauptfeld, $trainingsfeld){
    /*
    * Author: Simon
    * Since: 1.3.1 (red comments)
    * Comment: new since 1.3.1 is the check, if the querry runs successfull or not.
    */
    global $wpdb;
    
    $res01 = $wpdb->update('tbl_spielfeldstatus', array('status' => $hauptfeld), array('feldname' => 'Hauptfeld'));
    $res02 = $wpdb->update('tbl_spielfeldstatus', array('status' => $trainingsfeld), array('feldname' => 'Trainingsfeld'));
    if($res01 === false || $res02 === false){
        return "error";
    }
    return "erfolgreich";
}

/**
 * Function renamed in Version 1.3
 * add_scripts_and_css
 *
 * Adds the Bootstrap CSS Style URL to the head
 * 
 * @since 1.0.0
 *
 * @param (void)
 * @return (void)
 */
function add_scripts_and_css(){
    // IMPORT BOOTSTRAP CSS
    //removed in version 1.3
    //wp_register_style('prefix_bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');
    //wp_enqueue_style('prefix_bootstrap');
    //new in Version 1.3
    wp_enqueue_style( 'fch-plugin-bootstrap-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
}

function getSpielfeldStatusFormData(){
    // new in version 1.3.1
    $update_field_status = "";
    $redirurl = "";
    // end
    if (isset($_POST['hauptfeld']) || isset($_POST['trainingsfeld'])) {
        $update_field_status = updateSpielfeldStatus($_POST['hauptfeld'], $_POST['trainingsfeld']);
    }
    //New in Version 1.3
    if($update_field_status == "erfolgreich"){
        $redirurl = get_admin_url()."admin.php?page=fch_plugin&speichern=erfolgreich";
    } else {
        $redirurl = get_admin_url()."admin.php?page=fch_plugin&speichern=error";
    }
    wp_redirect( $redirurl );
}


// LOAD FUNCTION TO ADD THE BOOTSTRAP CSS URL
//removed in Version 1.3
//prefix_enqueue();
//new in Version 1.3
add_action( 'wp_enqueue_scripts', 'add_scripts_and_css' );

// LOAD SHORTCODE'S -> More information, https://codex.wordpress.org/Shortcode_API
add_shortcode('bandenwerber', 'wp_bandenwerber_shortcode');
add_shortcode('supporter', 'wp_supporter_shortcode');
add_shortcode('ehrenmitglieder', 'wp_ehrenmitglieder_shortcode');
add_shortcode('spielfeldstatus', 'wp_spielfeldstatus');

// LOAD ADMIN MENU
add_action( 'admin_menu', 'fch_plugin_setup_menu');

// DO POST ACTION
add_action('admin_post_nopriv_process_form', 'getSpielfeldStatusFormData');
add_action('admin_post_process_form', 'getSpielfeldStatusFormData');

?>




